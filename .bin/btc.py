#! /bin/python

import json
import requests

def curl(url):
    return requests.get(url).text

def jsonify(s):
    return json.loads(s)

def BTCfromSat(n):
    return n / 10 ** 8 # Satoshi -> BTC

def api_address(base_url, x, query='address'):
    return f'{base_url}/{query}/{x}'

def coin_rate(n=None, coin='btc'):
    inner = n if n else ''
    url = f'https://rate.sx/{inner}{coin}'
    return url

def get_satoshi(base_url, address):
    url = api_address(base_url, address)
    d = jsonify(curl(url))

    return d['data'][address]['address']['balance']

def to_usd(btc):
    url = coin_rate(btc)

    usd = curl(url).strip()
    usd = round(float(usd), 2)

    return usd

ADDRESS = 'bc1qeezgp0gx3w9e5kutq0k9e92pxqcc2s5rex73pf'
BASE_URL = 'https://api.blockchair.com/bitcoin/dashboards'

as_btc = BTCfromSat(get_satoshi(BASE_URL, ADDRESS))
as_usd = to_usd(as_btc)

print(as_btc, as_usd)
